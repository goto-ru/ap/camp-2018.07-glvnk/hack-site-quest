from django.contrib import admin

# Register your models here.
from main.models import UserStats, Quest

admin.site.register(UserStats)
admin.site.register(Quest)