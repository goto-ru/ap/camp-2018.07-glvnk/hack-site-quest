from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
# Create your views here.
from main.models import Quest, UserStats


def index(request):
    return redirect('/accounts/login')


@login_required
def userPersonalView(request):
    if request.user.is_staff:
        context = {'user': request.user}
        return render(request, 'main/dm_view.html', context)
    else:
        quest_list = Quest.objects.filter(user=request.user).filter(active=True)
        context = {'user': request.user, 'quest_list': quest_list}
        return render(request, 'main/userView.html', context)


@login_required
def create_quest_view(request):
    if not request.user.is_staff:
        context = {}
        return render(request, 'main/error_dm.html', context)
    else:
        if request.method == 'POST':
            if request.POST.get('quest_name', '') == '':  # "Not quest" shitty-check. Well, i had only 24h :(
                q_id = request.POST['id']
                q_verdict = request.POST['verdict']
                q = Quest.objects.get(id=q_id)
                u = User.objects.get(username=q.user.username)
                if q_verdict == 'accepted':
                    u.userstats.gold += q.rewardGold
                    u.userstats.XP += 10*u.userstats.level + q.rewardXP
                    u.userstats.points += q.rewardPts
                    u.userstats.level = u.userstats.XP // 10  # 10xp = 1 lvl.
                    u.userstats.XP = u.userstats.XP % 10
                else:
                    u.userstats.HP -= q.penaltyHP
                q.active = False
                u.save()
                q.save()
            else:
                q = Quest()
                q.questName = request.POST['quest_name']
                q.rewardGold = request.POST['quest_gold']
                q.penaltyHP = request.POST['quest_hp']
                q.rewardXP = request.POST['quest_xp']
                q.rewardPts = request.POST['quest_pts']
                q.user = User.objects.get(username=request.POST['quest_user'])
                q.task = request.POST['quest_text']
                q.active = True
                q.save()
        quest_list = Quest.objects.filter(active=True)
        context = {'quest_list': quest_list}
        return render(request, 'main/dm_quests.html', context)


@login_required
def create_user_view(request):
    if not request.user.is_staff:
        context = {}
        return render(request, 'main/error_dm.html', context)
    else:
        if request.method == 'POST':
            u = User()
            u.username = request.POST['username']
            u.first_name = request.POST['u_fname']
            u.last_name = request.POST['u_lname']
            u.password = request.POST['u_pass']
            us = UserStats(user=u)
            u.userstats = us
            u.save()
            return redirect('/accounts/profile/')
        context = {}
        return render(request, 'main/dm_newpupil.html', context)


def leaderboard_view(request):

    users_list = User.objects.filter(is_staff=False).order_by('-userstats__points')
    # return render(request, 'main/dm_newpupil.html', {'users_list': users_list})

    sum_pts = 0
    for user in users_list:
        sum_pts += user.userstats.points
    context = {'users_list': users_list, 'team_sum': sum_pts}
    return render(request, 'main/leaderboard.html', context)


