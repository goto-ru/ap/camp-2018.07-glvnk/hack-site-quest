from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserStats(models.Model):
    HP = models.IntegerField(default=10)
    XP = models.IntegerField(default=0)
    level = models.IntegerField(default=1)
    points = models.IntegerField(default=0)
    gold = models.IntegerField(default=0)
    user = models.OneToOneField(User, on_delete=models.CASCADE)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserStats.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.userstats.save()


class Quest(models.Model):
    rewardXP = models.IntegerField(default=1)
    rewardGold = models.IntegerField(default=1)
    penaltyHP = models.IntegerField(default=1)
    rewardPts = models.IntegerField(default=1)
    task = models.TextField()
    questName = models.CharField(max_length=100)
    active = models.BooleanField(default=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
