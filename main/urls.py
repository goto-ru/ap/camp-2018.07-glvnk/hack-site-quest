from django.urls import path
from . import views

urlpatterns = [
    # ex: /polls/
    path('', views.index, name='index'),
    path('accounts/profile/', views.userPersonalView, name='user_profile'),
    path('accounts/createquest/', views.create_quest_view, name='create_quest'),
    path('accounts/createuser/', views.create_user_view, name='create_user'),
    path('leaderboard/', views.leaderboard_view, name='leaderboard')
]
